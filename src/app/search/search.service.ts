import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, Headers } from '@angular/http';
import { Observable, Subject, Observer } from 'rxjs/Rx';

const API = 'http://dev2.poiskznakov.ru/api-rest/test-me';

@Injectable()
export class SearchService {
	constructor(public http: Http) { }
	/*
		Получить результаты загрузки.
		На вход: тип поиска(не обязательно)
		На выход: json объект с результатами поиска от api
	*/
    public getSearchResult(searchType?: string) {
		const headers = new Headers();
		// Задать заголовки
		headers.append('Content-Type', 'application/json');
		let endPoint =  API;
		// Если задан тип поиска - добавить данную переменную в GET-запрос
		if (searchType) {
			endPoint =  API + '/?stype=' + searchType;
		}
		return this.http.get(endPoint, { headers: headers })
		.map((res: Response) => res.json())
		.catch((error: any) => Observable.throw(error.json() || 'Server error'));
    }

}

