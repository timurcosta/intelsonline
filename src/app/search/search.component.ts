import { Component, OnInit } from '@angular/core';
import { SearchService } from './search.service';
import * as moment from 'moment';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [SearchService]
})
export class SearchComponent implements OnInit {
  public searchResults: any;
  public selected: string = '';
  public loading: boolean;
  public connectLost: boolean;
  constructor(
    private searchService: SearchService,
  ) { }

  ngOnInit() {
    // Проверить наличие значения "типа поиска" в local storage.
    // Если есть - загружаем, если нет - значение по умолчанию
    if (localStorage['selected']) {
      this.selected = localStorage.getItem('selected');
    }
    // Загрузить результаты поиска при запуске приложения
    this.search(this.selected);
  }
  public search(selected) {
    // Включить флаг загрузки
    this.loading = true;
    // Вызвать фукнцию сервиса, возвращающую результаты поиска и подписаться на её обновления
    this.searchService.getSearchResult(selected)
    .subscribe(response => {
      
      /* В случае успешного выполнения */
      
      // Присвоить переменной результат выполнения
      this.searchResults = response;
      // Привести элементы объекта с датой начала поиска к желаемому виду (DD.MM.YYYY => YYYY-MM-DD)
      this.searchResults.forEach(searchResult => {
        searchResult.search_start = this.formatDate(searchResult.search_start);
      });
      // Отключить флаг загрузки
      this.loading = false;
      // Отключить флаг ошибки или разрыва соединения
      this.connectLost = false;
      // Сохранить в local storage значение "типа поиска"
      localStorage.setItem('selected', selected);

    }, err => {

       /* В случае ошибки */

      // отключить флаг загрузки
      this.loading = false;
      // Включить флаг ошибки или разрыва соединения
      this.connectLost = true;
  });

  }

  /* Функция переподлкючения к серверу в случае ошибки или разрыва соединения */
  public reconnect() {
    this.search(this.selected);
  }
  /* 
    Функция форматирования даты 
    на вход: DD.MM.YYYY
    на выход: YYYY-MM-DD
  */
  public formatDate(date): string {
    return moment(date, 'DD.MM.YYYY').format('YYYY-MM-DD');
  }

}
