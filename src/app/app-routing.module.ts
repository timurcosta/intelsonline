import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';

// Page Layouts

const AppRoutes: Routes = [
{ path: '', redirectTo: '/search', pathMatch: 'full' },
 { path: 'app', component: AppComponent },
  // { path: 'extra', loadChildren: './extra-pages/extra-pages.module#ExtraPagesModule' },
  { path: '**', redirectTo: '/search', pathMatch: 'full' },
];

export const AppRoutingModule = RouterModule.forRoot(AppRoutes, {useHash: false});
